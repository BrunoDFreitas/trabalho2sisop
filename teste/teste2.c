#include "../include/t2fs.h"
#include <stdio.h>

/**
 ** Programa cria 20 arquivos (de Teste_00 até Teste_19) na raiz
 ** e depois consulta todos os arquivos contidos neste diretório
 ** No final apaga todos os arquivos criados.
 **/

int main()
{
	FILE2 files[30];
    char nomeArq[10] = "Teste_00";

    int i;
    for(i = 0; i < 20; i++)
    {
        files[i] = create2(nomeArq);

        if(i == 9)
        {
            nomeArq[7] =  '0';
            nomeArq[6] = '1';
        }
        else
            nomeArq[7]++;
    }

    DIRENT2 dentry;
    DIR2 d = opendir2("/");

    printf("Estado do diretorio apos a criacao dos arquivos:\n");
 	while (readdir2(d, &dentry) == 0)
 	{
 		printf("Entry: Name => %s, Type => %s, Size => %lu.\n",
 			dentry.name,
 			(dentry.fileType == 0x01 ? "File" : "Directory"),
 			dentry.fileSize);
 	}
 	closedir2(d);


    for (i = 0; i < 20; ++i)
    	close2(files[i]);


    nomeArq[6] = '0';
    nomeArq[7] = '0';
    for(i = 0; i < 20; i++)
    {
        delete2(nomeArq);

        if(i == 9)
        {
            nomeArq[7] =  '0';
            nomeArq[6] = '1';
        }
        else
            nomeArq[7]++;
    }


    d = opendir2("/");
 	printf("\nEstado do diretorio apos a remocao dos arquivos:\n");
 	while (readdir2(d, &dentry) == 0)
 	{
 		printf("Entry: Name => %s, Type => %s, Size => %lu.\n",
 			dentry.name,
 			(dentry.fileType == 0x01 ? "File" : "Directory"),
 			dentry.fileSize);
 	}
 	closedir2(d);

	return 0;
}
