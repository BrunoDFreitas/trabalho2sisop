#include <stdlib.h>
#include <stdio.h>

struct t {
    char id[4];
    short int s;
    int i;
} __attribute__((packed));



int main()
{
    // z recebe o endereço da struct alocada em memória
    int z = malloc(sizeof(struct t));

    // ponteiro x aponta para a estrutura criada anteriormente
    struct t *x = z;
    x->id[0] = 'a';
    x->id[1] = 'b';
    x->id[2] = 'c';
    x->id[3] = 'd';
    x->s = 1;
    x->i = 2;

    printf("%c, %c, %c, %c, %d, %d\n"
           , x->id[0], x->id[1], x->id[2], x->id[3]
           , x->s, x->i);


    return 0;
}
