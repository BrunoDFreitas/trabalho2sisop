#include "../include/t2fs.h"
#include <stdio.h>

/**
 **
 ** Este programa serve para testar a criação de subdiretórios.
 ** Serão criados três níveis de subdiretórios e um arquivo em
 ** cada um.
 ** Arvore de diretórios resultante
 ** ---- /
 ** ---- arq1
 ** ---- dir1
 ** ---- ---- arq2
 ** ---- ---- dir2
 ** ---- ---- ---- arq3
 ** ---- ---- ---- dir3
 **
 ** Observação: o disco deve estar limpo para obter o resultado acima.
 **
 ** Foi utilizado para a criação de alguns diretórios caminhos absolutos
 ** e relativos.
 **
 **/

int main()
{
	DIRENT2 dentry;
	DIR2 d;

	FILE2 f1 = create2("Arq1");
	mkdir2("/dir1");
	chdir2("/dir1");

	FILE2 f2 = create2("arq2");
	mkdir2("../dir1/dir2");
	chdir2("/./dir1/dir2");

	FILE2 f3 = create2("arq3");
	chdir2("/");

	d = opendir2("/");


	printf("Lendo diretorio '/'\n");
	while (readdir2(d, &dentry) == 0)
	{
		printf("Entry: Name => %s, Type => %s, Size => %lu.\n",
 			dentry.name,
 			(dentry.fileType == 0x01 ? "File" : "Directory"),
 			dentry.fileSize);
	}
	closedir2(d);

	chdir2("./dir1");
	d = opendir2(".");
	printf("Lendo '/dir1'\n");
	while (readdir2(d, &dentry) == 0)
	{
		printf("Entry: Name => %s, Type => %s, Size => %lu.\n",
 			dentry.name,
 			(dentry.fileType == 0x01 ? "File" : "Directory"),
 			dentry.fileSize);
	}
	closedir2(d);

	chdir2("/dir1");
	mkdir2("./dir2/dir3");
	if ((d = opendir2("./dir2")) < 0) {
		printf("Erro ao ler '/dir1/dir2'\n");
	}
	else
	{
		printf("Lendo '/dir1/dir2'\n");
		while (readdir2(d, &dentry) == 0)
		{
			printf("Entry: Name => %s, Type => %s, Size => %lu.\n",
				dentry.name,
				(dentry.fileType == 0x01 ? "File" : "Directory"),
				dentry.fileSize);
		}
	}
	return 0;
}
