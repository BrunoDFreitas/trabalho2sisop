#include "../include/t2fs.h"
#include <stdio.h>

/**
 ** Programa para testar a criação, escrita, alteração do offset,
 ** leitura de um arquivo e a remoção do mesmo.
 **
 **/


int main()
{
    int size = 255;
    char nomes[255];
    identify2(nomes, size);
    printf("Identificacao do grupo: %s\n\n", nomes);

	FILE2 handle = create2("arquivo_teste.txt");

    char buffer[] = "Estamos escrevendo no arquivo!\nEsta parte do texto comeca na posicao 31!\n";
    write2(handle, buffer, sizeof(buffer));
    seek2(handle, 0);
    char read[1024];
    read2(handle, read, sizeof(read));
    printf("Texto contido no arquivo:\n");
    printf("%s\n", read);

    seek2(handle, 31);
    read2(handle, read, sizeof(read));
    printf("Texto contido no arquivo a partir da posicao 31:\n");
    printf("%s\n", read);

    char buffer2[] = "Agora sobrescrevemos o texto contido na posicao 31!\n";
    seek2(handle, 31);
    write2(handle, buffer2, sizeof(buffer2));
    seek2(handle, 0);
    read2(handle, read, sizeof(read));
    printf("Sobrescrevemos parte do texto:\n");
    printf("%s\n", read);

    char buffer3[] = "Acrescentamos coisas no final agora!\n";
    seek2(handle, -1);
    write2(handle, buffer3, sizeof(buffer3));
    seek2(handle, 0);
    read2(handle, read, sizeof(read));
    printf("Acrescentamos novas coisas no texto:\n");
    printf("%s\n", read);

    close2(handle);
    handle = open2("arquivo_teste.txt");
    read2(handle, read, sizeof(read));
    printf("Leitura do arquivo apos reabri-lo:\n");
    printf("%s\n", read);

    DIRENT2 dentry;
    DIR2 d = opendir2("/");

 	printf("Arquivos do diretorio '/' antes da remocao do arquivo:\n");
    while (readdir2(d, &dentry) == 0)
 	{
 		printf("Entry: Name => %s, Type => %s, Size => %lu.\n",
        dentry.name,
        (dentry.fileType == 0x01 ? "File" : "Directory"),
        dentry.fileSize);
 	}
    closedir2(d);

    close2(handle);
    delete2("arquivo_teste.txt");

    d = opendir2("/");
    printf("\nArquivos do diretorio '/' apos a remocao:\n");
    while (readdir2(d, &dentry) == 0)
 	{
 		printf("Entry: Name => %s, Type => %s, Size => %lu.\n",
        dentry.name,
        (dentry.fileType == 0x01 ? "File" : "Directory"),
        dentry.fileSize);
 	}
    return 0;
}
