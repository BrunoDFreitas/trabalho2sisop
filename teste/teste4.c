#include "../include/t2fs.h"
#include <stdio.h>

/**
 **
 ** Este programa serve para testar a criação e remoção de subdiretórios.
 ** Serão criados três níveis de subdiretórios conforme a árvore abaixo.
 ** Depois será apagado todos os diretórios.
 ** ---- /
 ** ---- dir1
 ** ---- dir2
 ** ---- ---- dir3
 ** ---- ---- dir4
 ** ---- ---- ---- dir5
 ** ---- ---- ---- dir6
 **
 ** Observação: o disco deve estar limpo para obter o resultado acima.
 **
 ** Foi utilizado para a criação/remoção de alguns diretórios caminhos
 ** absolutos e relativos.
 **
 **/

int main()
{
	DIRENT2 dentry;
	DIR2 d;

	mkdir2("/dir1"); // Caminho absoluto
	mkdir2("dir2");  // Caminho relativo

	mkdir2("/dir2/dir3"); // Caminho absoluto
	mkdir2("dir2/dir4");  // Caminho relativo

	mkdir2("/dir2/dir4/dir5"); // Caminho absoluto
	chdir2("/dir2");
	mkdir2("dir4/dir6");  // Caminho relativo

	chdir2("/");
	d = opendir2("/");

    int size = 255;
    char dirAtual[255];

    printf("\n\nDiretorios apos as criacoes!\n\n");

    getcwd2(dirAtual, size);
    printf("Lendo diretorio '%s'\n", dirAtual);
	while (readdir2(d, &dentry) == 0)
	{
		printf("Entry: Name => %s, Type => %s, Size => %lu.\n",
 			dentry.name,
 			(dentry.fileType == 0x01 ? "File" : "Directory"),
 			dentry.fileSize);
	}
	closedir2(d);

	chdir2("./dir2");
	d = opendir2(".");
	getcwd2(dirAtual, size);
    printf("\nLendo diretorio '%s'\n", dirAtual);
	while (readdir2(d, &dentry) == 0)
	{
		printf("Entry: Name => %s, Type => %s, Size => %lu.\n",
 			dentry.name,
 			(dentry.fileType == 0x01 ? "File" : "Directory"),
 			dentry.fileSize);
	}
	closedir2(d);

	d = opendir2("./dir4");
    chdir2("/dir2/dir4");
    getcwd2(dirAtual, size);
    printf("\nLendo diretorio '%s'\n", dirAtual);
	while (readdir2(d, &dentry) == 0)
    {
        printf("Entry: Name => %s, Type => %s, Size => %lu.\n",
            dentry.name,
            (dentry.fileType == 0x01 ? "File" : "Directory"),
            dentry.fileSize);
    }
    closedir2(d);

    /********************/
    /***** REMOÇÕES *****/
    /********************/

    printf("\n\nEtapa de Remocao!\n\n");

    getcwd2(dirAtual, size);
    rmdir2("dir6");
    rmdir2("/dir2/dir4/dir5");
    d = opendir2(".");

    printf("\nDiretorio '%s' apos as remocoes\n", dirAtual);
	while (readdir2(d, &dentry) == 0)
    {
        printf("Entry: Name => %s, Type => %s, Size => %lu.\n",
            dentry.name,
            (dentry.fileType == 0x01 ? "File" : "Directory"),
            dentry.fileSize);
    }
    closedir2(d);


	chdir2("..");
    getcwd2(dirAtual, size);
    rmdir2("dir4");
    rmdir2("./dir3");
    d = opendir2(".");

    printf("\nDiretorio '%s' apos as remocoes\n", dirAtual);
	while (readdir2(d, &dentry) == 0)
    {
        printf("Entry: Name => %s, Type => %s, Size => %lu.\n",
            dentry.name,
            (dentry.fileType == 0x01 ? "File" : "Directory"),
            dentry.fileSize);
    }
    closedir2(d);


	chdir2("..");
    getcwd2(dirAtual, size);
    rmdir2("dir2");
    rmdir2("./dir1");
    d = opendir2(".");

    printf("\nDiretorio '%s' apos as remocoes\n", dirAtual);
	while (readdir2(d, &dentry) == 0)
    {
        printf("Entry: Name => %s, Type => %s, Size => %lu.\n",
            dentry.name,
            (dentry.fileType == 0x01 ? "File" : "Directory"),
            dentry.fileSize);
    }
    closedir2(d);


    return 0;
}

