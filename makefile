CC=gcc
LIB_DIR=./lib
INC_DIR=./include
BIN_DIR=./bin
SRC_DIR=./src
TST_DIR=./teste

all: compauxt2fs geralib 

compauxt2fs:
	$(CC) -c $(SRC_DIR)/auxiliar.c -Wall
	$(CC) -c $(SRC_DIR)/t2fs.c -Wall

geralib:
	ar crs $(LIB_DIR)libt2fs.a auxiliar.o t2fs.o $(LIB_DIR)/apidisk.o

clean:
	rm -rf $(LIB_DIR)/*.a $(BIN_DIR)/*.o $(SRC_DIR)/*~ $(INC_DIR)/*~  ./*.o


