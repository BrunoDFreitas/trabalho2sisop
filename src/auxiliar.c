#include "../include/apidisk.h"
#include "../include/auxiliar.h"
#include "../include/t2fs.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/*** Descomentar aqui para debugar **/
//#define AUX_DEBUG

/// Funções privadas
int __gravaRecord(struct t2fs_record *record, BYTE *bloco, int posBloco, int nroBloco);
int __limpaRecord(struct t2fs_record *record);
int __limpaInode(int nroInode);
void __carregaRecordDoBloco(struct t2fs_record *recordDest, BYTE *bloco, int nroRecord);



//Função que lẽ o super bloco do disco sendo usado e salva na variável global. Retorno: 0=leu com sucesso; -1=erro na leitura
//REMOVER NA FINAL: printfs
int leSuperbloco()
{
    superBloco = (struct t2fs_superbloco *)malloc(SECTOR_SIZE);
    if (read_sector(0, (char*) superBloco) != 0)
        return -1;

    setoresPorBloco = superBloco->BlockSize/SECTOR_SIZE;

    maxPtrIndiretosPorBloco = superBloco->BlockSize/sizeof(int);

    nroMaxBlocos = 10 + maxPtrIndiretosPorBloco + maxPtrIndiretosPorBloco*maxPtrIndiretosPorBloco;

    nroMaxBytes = nroMaxBlocos * (superBloco->BlockSize);

    setorDiretorioCorrente = 0; // Raiz '/'
    bytesBitmapBlocos = (superBloco->NofBlocks)/8;

    int nroBlocosInodes = superBloco->FirstDataBlock - superBloco->InodeBlock;
    int tamBlocosInodes = nroBlocosInodes * superBloco->BlockSize;
    int nroMaxInodes = tamBlocosInodes/sizeof(struct t2fs_inode);
    bytesBitmapInodes = nroMaxInodes/8;

    inodesPorBloco = superBloco->BlockSize/(sizeof(struct t2fs_inode));
    recordPorBloco = superBloco->BlockSize/(sizeof(struct t2fs_record));

    nroInodeDirAtual = 0; // Inode do diretório raiz

    if(loadBitMapBlocks() != 0)
        return -1;

    if(loadBitMapInodes())
        return -1;

    #ifdef AUX_DEBUG
    printf("Tamanho Super Bloco: %d blocos\n",superBloco->SuperBlockSize);
    printf("Tamanho do Disco: %d bytes\n",superBloco->DiskSize);
    printf("Numero de Blocos: %d blocos\n",superBloco->NofBlocks);
    printf("Tamanho do Bloco: %d bytes\n",superBloco->BlockSize);
    printf("Primeiro Bloco de Bitmap de Blocos: %d\n",superBloco->BitmapBlocks);
    printf("Primeiro Bloco de Bloco Bitmap de Inodes: %d\n",superBloco->BitmapInodes);
    printf("Primeiro Bloco de Inodes: %d\n",superBloco->InodeBlock);
    printf("Primeiro Bloco de Dados: %d\n",superBloco->FirstDataBlock);

    printf("Setores por bloco: %d\n", setoresPorBloco);
    printf("Entradas de blocos indexados: %d\n", maxPtrIndiretosPorBloco);
    printf("Numero Maximo de Blocos: %d\n", nroMaxBlocos);
    printf("Numero Maximo de bytes por arquivo: %d\n", nroMaxBytes);
    printf("Bytes necessario para o bitmap de blocos: %d bytes \n", bytesBitmapBlocos);
    printf("Bytes necessario para o bitmap de inodes: %d bytes \n", bytesBitmapInodes);
    printf("Inodes por bloco: %d\n", inodesPorBloco);

    statusBitmapBlocks();
    statusBitmapInodes();
    #endif

    int i;
    for(i = 0; i < 20; i++)
    {
        arqAberto[i] = (ARQ_ABERTO) { .aberto = false };
    }

    for(i = 0; i < 20; i++)
    {
        dirAberto[i] = (DIR_ABERTO) { .aberto = false };
    }

    return 0;
}


//Função que lẽ um bloco do disco e coloca-o em um buffer.
//Retorno: 0=leu com sucesso; -1=erro na leitura
int le_bloco(unsigned int bloco, char *buffer)
{
	int spb=setoresPorBloco;//setores por bloco
	int i;

	for(i=0; i < spb; i++)
	{
		if(read_sector((bloco*spb)+i, &buffer[SECTOR_SIZE*i]) != 0)
            return erroMensagem("Erro ao ler o bloco!", "bloco_para_buffer");
	}
	return 0;
}


//Função que lẽ um bloco do disco e coloca-o em um buffer.
//Retorno: 0=leu com sucesso; -1=erro na leitura
int grava_bloco(unsigned int bloco, char *buffer)
{
	int spb=setoresPorBloco;//setores por bloco
	int i;

	for(i=0; i < spb; i++)
	{
		if(write_sector((bloco*spb)+i, &buffer[SECTOR_SIZE*i]) != 0)
            return erroMensagem("Erro ao gravar o bloco!", "buffer_para_bloco");
	}
	return 0;
}


// Função que carrega um novo record para criação de um arquivo/diretório
// Retorno: 0 = carregou com sucesso, -1 = erro no carregamento
// IMPORTANTE: fazer o malloc antes de chamar esta função
int iniciaNovoRecord(struct t2fs_record *record, BYTE type, char *filename)
{
    int posInode = inodeLivre();
    if(posInode == -1)
        return -1;

    record->TypeVal = type;
    strncpy(record->name, filename, sizeof(record->name));


    record->blocksFileSize = 0;
    record->bytesFileSize = 0;
    record->i_node = posInode;

    limpaBuffer(record->Reserved, 16);

    atualizaBitmapInode(posInode, 1);
    criaInode(record->i_node);
    return 0;
}


// Função que apaga os valores de um record.
// Retorno: 0 = apagou com sucesso, -1 = erro
int __limpaRecord(struct t2fs_record *record)
{
    if (atualizaBitmapInode(record->i_node, 0)!=0) return erroMensagem("Erro ao atualizar bitmap de inodes!", "__limpaRecord");

    record->TypeVal = 0;
    limpaBuffer(record->name, 31);

    record->blocksFileSize = 0;
    record->bytesFileSize = 0;

    __limpaInode(record->i_node);
    record->i_node = 0;

    limpaBuffer(record->Reserved, 16);

    return 0;
}


int __limpaInode(int nroInode)
{
    struct t2fs_inode *inode = malloc(sizeof(struct t2fs_inode));
    if(carregaInode(inode, nroInode) != 0) return -1;

    int ptr = 0;
    while(ptr < 10)
    {
        if(inode->dataPtr[ptr] != PTR_INVALIDO)
        {
            atualizaBitmapBlocos(inode->dataPtr[ptr], 0);
        }
        ptr++;
    }
    return 0;
}

// Função que cria um inode nos blocos de inodes com valores padrões.
// Retorno: 0 = inode criado com sucesso, -1 = erro
int criaInode(int posInode)
{
    BYTE *bloco = malloc(superBloco->BlockSize);

    int i = 0;
    while(posInode >= inodesPorBloco)
    {
        i++;
        posInode -= inodesPorBloco;
    }

    if(le_bloco(superBloco->InodeBlock+i, (char *) bloco) != 0)
        return erroMensagem("Erro ao ler o bloco de inodes!", "criaInode(int posInode)");

    struct t2fs_inode *inode = malloc(sizeof(struct t2fs_inode));

    int j;
    for(j = 0; j < 10; j++)
        inode->dataPtr[j] = PTR_INVALIDO;
    inode->singleIndPtr = PTR_INVALIDO;
    inode->doubleIndPtr = PTR_INVALIDO;

    int posGravacao = posInode*(sizeof(struct t2fs_inode));
    copiaByteAByte((char *) &bloco[posGravacao], (char *)inode, 0, sizeof(struct t2fs_inode));

    if(grava_bloco(superBloco->InodeBlock+i, (char *) bloco) != 0)
        return erroMensagem("Erro ao gravar o bloco de inodes!", "criaInode(int posInode)");

    free(inode);
    free(bloco);
    return 0;
}

// Função que remove um i-node.
// Retorno: 0 = Sucesso na remoção, -1 =  Erro
int removeInode(int posInode)
{
    int posOriginal = posInode;
    struct t2fs_inode *inode = malloc(sizeof(struct t2fs_inode));
    if(carregaInode(inode, posInode) != 0)
        return erroMensagem("Erro ao carregar o inodes!", "removeInode"); ;

    BYTE *bloco = malloc(superBloco->BlockSize);

    int i = 0;
    while(posInode >= inodesPorBloco)
    {
        i++;
        posInode -= inodesPorBloco;
    }

    if(le_bloco(superBloco->InodeBlock+i, (char *) bloco) != 0)
        return erroMensagem("Erro ao ler o bloco de inodes!", "removeInode");

    int j;
    for(j = 0; j < 10; j++)
    {
        if(inode->dataPtr[j] != PTR_INVALIDO)
        {
            atualizaBitmapBlocos(inode->dataPtr[j], 0);
            inode->dataPtr[j] = PTR_INVALIDO;
        }
    }

    if(inode->singleIndPtr != PTR_INVALIDO)
    {
        /*** TODO -> Remover Inodes indiretos **/
        inode->singleIndPtr = PTR_INVALIDO;
    }

    if(inode->doubleIndPtr != PTR_INVALIDO)
    {
        /*** TODO -> Remover Inodes indiretos **/
        inode->doubleIndPtr = PTR_INVALIDO;
    }

    int posGravacao = posInode*(sizeof(struct t2fs_inode));
    copiaByteAByte((char *) &bloco[posGravacao], (char *)inode, 0, sizeof(struct t2fs_inode));

    if(grava_bloco(superBloco->InodeBlock+i, (char *) bloco) != 0)
        return erroMensagem("Erro ao gravar o bloco de inodes!", "removeInode");

    atualizaBitmapInode(posOriginal, 0);

    free(inode);
    free(bloco);
    return 0;
}


// Função recebe a posição do bitmap de Inodes e atualiza se está ocupado (1) ou não (0)
// Retorno: 0 = bitmap de inodes atualizado com sucesso, -1 = erro
int atualizaBitmapInode(int pos, int ocupado)
{
    alteraBit(BitMap_inodes, pos, ocupado);

    if(grava_bloco(superBloco->BitmapInodes, (char *)BitMap_inodes) != 0)
        return erroMensagem("Erro ao gravar o bitmap de inodes!", "atualizaBitmapInode");

    return 0;
}


// Função recebe a posição do bitmap de blocos e atualiza se está ocupado (1) ou não (0)
// Retorno: 0 = bitmap de blocos atualizado com sucesso, -1 = erro
int atualizaBitmapBlocos(int pos, int ocupado)
{
    alteraBit(BitMap_Blocos, pos, ocupado);

    if(grava_bloco(superBloco->BitmapBlocks, (char *)BitMap_Blocos) != 0)
        return erroMensagem("Erro ao gravar o bitmap de blocos!", "atualizaBitmapBlocos");

    return 0;
}


// Função que altera o bit da posição pos do bitmap *c para o valor dado
// Retorno: não há retorno
void alteraBit(unsigned char *c, int pos, char valor)
{
    int byte = pos/8;
    char bit = (char) (pos-8*byte);
    if(valor == 1)
        c[byte] |= (1u << bit);
    if(valor == 0)
        c[byte] ^= (1u << bit);
}


// Função carrega inode de número nroInode.
//Retorno: 0 = carregou inode com sucesso, -1 = erro
int carregaInode(struct t2fs_inode *inode, int nroInode)
{
    int nroBloco = 0;
    while(nroInode >= inodesPorBloco)
    {
        nroBloco++;
        nroInode -= inodesPorBloco;
    }

    // Valida se o nroBloco não é maior que o número total de blocos
    if(nroBloco >= (superBloco->FirstDataBlock - superBloco->InodeBlock))
        return erroMensagem("Numero de inode invalido", "carregaInode");

    BYTE *bloco = malloc(superBloco->BlockSize);
    if(le_bloco(superBloco->InodeBlock + nroBloco, (char *) bloco))
        return erroMensagem("Erro ao ler o bloco!", "carregaInode");

    copiaByteAByte((char *) inode, (char *) bloco, nroInode*(sizeof(struct t2fs_inode)), sizeof(struct t2fs_inode));
    free(bloco);
    return 0;
}


// Função que grava um inode no lugar de nroInode (usa as informações da variável inode
// Retorno: 0 = gravou com sucesso, -1 = erro na gravação
int gravaInode(struct t2fs_inode *inode, int nroInode)
{
    int nroBloco = 0;
    while(nroInode >= inodesPorBloco)
    {
        nroBloco++;
        nroInode -= inodesPorBloco;
    }


    // Valida se o nroBloco não é maior que o número total de blocos
    if(nroBloco >= (superBloco->FirstDataBlock - superBloco->InodeBlock))
        return erroMensagem("Numero de inode invalido", "gravaInode");


    BYTE *bloco = malloc(superBloco->BlockSize);
    if(le_bloco(superBloco->InodeBlock + nroBloco, (char *) bloco))
        return erroMensagem("Erro ao ler o bloco!", "gravaInode");

    copiaByteAByte((char *) &bloco[nroInode*sizeof(struct t2fs_inode)], (char *) inode, 0, sizeof(struct t2fs_inode));

    if(grava_bloco(superBloco->InodeBlock + nroBloco, (char *) bloco))
        return erroMensagem("Erro ao gravar o bloco!", "gravaInode");

    free(bloco);
    return 0;
}


// Função utilizada para contar o número arquivos do diretório dirInode
// Caso bool seja true, imprime os dados do record dos arquivos
// Retorno: n = número de arquivos encontrados, -1 = erro na leitura
int contaArquivos(int dirInode, char imprimir)
{
    int count = 0;
    struct t2fs_inode *inode = malloc(sizeof(struct t2fs_inode));

    if(carregaInode(inode, dirInode) != 0)
        return erroMensagem("Erro ao carregar o inode!", "contaArquivos");

    BYTE *bloco = malloc(superBloco->BlockSize);

    int ptr = 0;

    while(ptr < 10)
    {
        if(inode->dataPtr[ptr] != PTR_INVALIDO)
        {
            int nroBloco = inode->dataPtr[ptr];

            if(le_bloco(nroBloco, (char *) bloco))
                return erroMensagem("Erro ao ler o bloco!", "contaArquivos");

            struct t2fs_record *record = malloc(sizeof(struct t2fs_record));

            int i=0;
            while(i < recordPorBloco)
            {
                copiaByteAByte((char *) record, (char *) bloco, i*sizeof(struct t2fs_record), sizeof(struct t2fs_record));
                if((record->TypeVal == TYPEVAL_REGULAR || record->TypeVal == TYPEVAL_DIRETORIO))
                {
                    if(imprimir)
                        imprimeRecord(record);

                    count++;
                    i++;
                }
                else
                {
                    i++;
                }
            }

            free(record);
        }
        ptr++;
    }

    //indireção simples
    if(inode->singleIndPtr != PTR_INVALIDO)
    {
        BYTE *temp = malloc(superBloco->BlockSize);

        if(le_bloco(inode->singleIndPtr, (char *) bloco))
                return erroMensagem("Erro ao ler o bloco!", "contaArquivos");

        int j=0;

        while(j<maxPtrIndiretosPorBloco)
        {
            if(bloco[j] != PTR_INVALIDO)
            {
                if(le_bloco(bloco[j], (char *) temp))
                    return erroMensagem("Erro ao ler o bloco!", "contaArquivos");

                struct t2fs_record *record = malloc(sizeof(struct t2fs_record));

                int i=0;
                while(i < recordPorBloco)
                {
                    copiaByteAByte((char *) record, (char *) temp, i*sizeof(struct t2fs_record), sizeof(struct t2fs_record));
                    if((record->TypeVal == TYPEVAL_REGULAR || record->TypeVal == TYPEVAL_DIRETORIO))
                    {
                        if(imprimir)
                            imprimeRecord(record);

                        count++;
                        i++;
                    }
                    else
                    {
                        i++;
                    }
                }
                free(record);
            }
            j++;

        }
        free(temp);

    }

    //Ponteiros indiretos duplos

    if(inode->doubleIndPtr != PTR_INVALIDO)
    {
        BYTE *temp = malloc(superBloco->BlockSize);
        BYTE *temp2 = malloc(superBloco->BlockSize);

        if(le_bloco(inode->doubleIndPtr, (char *) bloco))
                return erroMensagem("Erro ao ler o bloco de indireção dupla!", "contaArquivos");

        int k=0;

        while(k<maxPtrIndiretosPorBloco)
        {
            if(bloco[k] != PTR_INVALIDO)
            {
                if(le_bloco(bloco[k], (char *) temp))
                    return erroMensagem("Erro ao ler o bloco de indireção dupla nivel 2!", "contaArquivos");

                int j=0;

                while(j<maxPtrIndiretosPorBloco)
                {

                    if(le_bloco(bloco[j], (char *) temp2))
                    return erroMensagem("Erro ao ler o bloco de indireção dupla nivel de dados!", "contaArquivos");

                    struct t2fs_record *record = malloc(sizeof(struct t2fs_record));

                    int i=0;
                    while(i < recordPorBloco)
                    {
                        copiaByteAByte((char *) record, (char *) temp2, i*sizeof(struct t2fs_record), sizeof(struct t2fs_record));
                        if((record->TypeVal == TYPEVAL_REGULAR || record->TypeVal == TYPEVAL_DIRETORIO))
                        {
                            if(imprimir)
                                imprimeRecord(record);

                            count++;
                            i++;
                        }
                        else
                        {
                            i++;
                        }
                    }
                    free(record);
                    j++;
                }

            }
            k++;
        }

        free(temp);
        free(temp2);
    }

    /*****************************************************************/
    /*** TODO -> Carregar os blocos dos ponteiros indiretos: FEITO ***/
    /******************************************************************/
    free(inode);
    free(bloco);
    return count;
}


// Função que lista os arquivos da primeira indireção.
// Entrada: bloco na qual o ponteiro simples aponta
// Retorno: 0=sucesso; -1=erro
int indirSimples_listaArquivos(int nroBlocoPrt)
{
    int *blocoPonteiro = malloc(sizeof(int)*maxPtrIndiretosPorBloco); // Os dois blocos tem o mesmo tamanho, mas usando int evita necessidade de cast
    BYTE *bloco = malloc(superBloco->BlockSize);

    if(le_bloco(nroBlocoPrt, (char *) blocoPonteiro))
        return erroMensagem("Erro ao ler o bloco de ponteiros indireto!", "indirSimples_listaArquivos");

    //ponteiros diretos normais
    int nroBloco;
    int ptr = 0;
    while(ptr < maxPtrIndiretosPorBloco)
    {
        nroBloco = blocoPonteiro[ptr];

        if(nroBloco != PTR_INVALIDO)
        {
            if(le_bloco(nroBloco, (char *) bloco))
                return erroMensagem("Erro ao ler o bloco!", "indirSimples_listaArquivos");

            struct t2fs_record *record = malloc(sizeof(struct t2fs_record));

            int i=0;
            while(i < recordPorBloco)
            {
                copiaByteAByte((char *) record, (char *) bloco, i*sizeof(struct t2fs_record), sizeof(struct t2fs_record));
                if((record->TypeVal == TYPEVAL_REGULAR || record->TypeVal == TYPEVAL_DIRETORIO))
                {
                    printf("\n");
                    printf("Tipo de Arquivo: %u\n", record->TypeVal);
                    printf("Nome: %s\n", record->name);
                    printf("Tamanho em Blocos: %u blocos\n", record->blocksFileSize);
                    printf("Tamanho em Bytes: %u byrtes\n", record->bytesFileSize);
                    printf("Inode associado: %u\n", record->i_node);
                    i++;
                }
                else
                {
                    i++;
                }
            }

            free(record);
        }
        ptr++;
    }

    free(blocoPonteiro);
    free(bloco);
    return 0;
}


//Função que lista os arquivos da primeira indireção.
//Retorno: 0=sucesso; -1=erro
int indirDuplo_listaArquivos(int nroBlocoPrt)
{
    int *blocoPonteiro = malloc(sizeof(int)*maxPtrIndiretosPorBloco); // Os dois blocos tem o mesmo tamanho, mas usando int evita necessidade de cast

    if(le_bloco(nroBlocoPrt, (char *) blocoPonteiro))
        return erroMensagem("Erro ao ler o bloco de ponteiros indiretos duplo!", "indirDuplo_listaArquivos");

    int nroBloco;
    //ponteiros diretos normais
    int ptr = 0;
    while(ptr < maxPtrIndiretosPorBloco)
    {
        nroBloco = blocoPonteiro[ptr];

        if(nroBloco != PTR_INVALIDO)
        {
            if(indirSimples_listaArquivos(nroBloco) != 0)
                return erroMensagem("Erro ao ler o bloco de ponteiros indiretos!", "indirDuplo_listaArquivos");
        }
        ptr++;
    }

    free(blocoPonteiro);
    return 0;
}


// Função privada para gravar o record na memória
//Retorno: 0 = sucesso, -1 = erro
int __gravaRecord(struct t2fs_record *record, BYTE *bloco, int nroRecord, int nroBloco)
{
    // Copia o novo record para o bloco
    copiaByteAByte((char *) &bloco[nroRecord*sizeof(struct t2fs_record)], (char *) record, 0, sizeof(struct t2fs_record));

    if(grava_bloco(nroBloco, (char *) bloco) != 0)
        return erroMensagem("Erro ao gravar o bloco", "__gravaRecord");

    return 0;
}


// Função para gravar record no diretório dirInode
// Retorno: 0 =  sucesso, -1 = erro
int gravaRecord(struct t2fs_record *record, int dirInode)
{
    struct t2fs_inode *inode = malloc(sizeof(struct t2fs_inode));

    if(carregaInode(inode, dirInode) != 0)
        return -1;

    BYTE *bloco = malloc(superBloco->BlockSize);

    int ptr = 0;
    bool parar = false; // Para de procurar caso ache espaço para gravar

    while(ptr < 10 && !parar)
    {
        // Bloco existente
        if(inode->dataPtr[ptr] != PTR_INVALIDO)
        {
            int nroBloco = inode->dataPtr[ptr];

            if(le_bloco(nroBloco, (char *) bloco))
                return -1;

            struct t2fs_record *recordAux = malloc(sizeof(struct t2fs_record));

            int nroRecord=0;
            while(nroRecord < recordPorBloco && !parar)
            {
                __carregaRecordDoBloco(recordAux, bloco, nroRecord);
                if((recordAux->TypeVal == TYPEVAL_REGULAR || recordAux->TypeVal == TYPEVAL_DIRETORIO))
                {
                    nroRecord++;
                }
                else
                {
                    parar = true;
                    if(__gravaRecord(record, bloco, nroRecord, nroBloco) != 0)
                        return erroMensagem("Erro ao gravar o record!", "gravaRecord");
                }
            }

            free(recordAux);
            ptr++;
        }
        // Criar novo bloco para o dataPtr[ptr]
        else
        {
            int novoBlocoDados = blocoDadosLivre();

            if(novoBlocoDados == -1)
                return erroMensagem("Blocos de dados informado eh invalido!", "gravaRecord");

            atualizaBitmapBlocos(novoBlocoDados, 1);

            limpaBuffer((char *)bloco, superBloco->BlockSize);

            inode->dataPtr[ptr] = novoBlocoDados;
            if(__gravaRecord(record, bloco, 0, novoBlocoDados) != 0)
                return erroMensagem("Erro ao gravar o record!", "gravaRecord");

            gravaInode(inode, dirInode);

            parar = true;
        }
    }

    /**********************************************************/
    /*** TODO -> Gravar no bloco de ponteiros indiretos     ***/
    /**********************************************************/
    if(!parar)
        return erroMensagem("Nao ha espaco para gravar!", "gravaRecord");

    free(inode);
    free(bloco);
    return 0;
}


// Função que remove o arquivo/diretorio do diretório atual de acordo com o tipo
//Retorno: 0=sucesso na remoção, -1=erro na remoção
int removeRecord(char* filename, int dirInode, int tipo)
{
    struct t2fs_inode *inode = malloc(sizeof(struct t2fs_inode));

    if(carregaInode(inode, dirInode) != 0)
        return -1;

    BYTE *bloco = malloc(superBloco->BlockSize);
    struct t2fs_record *record = malloc(sizeof(struct t2fs_record));

    int ptr = 0;
    bool parar = false;
    bool vazio;

    while(ptr < 10 && !parar)
    {
        if(inode->dataPtr[ptr] != PTR_INVALIDO)
        {
            int nroBloco = inode->dataPtr[ptr];

            if(le_bloco(nroBloco, (char *) bloco))
                return -1;

            int nroRecord=0;
            vazio = true;
            while(nroRecord < recordPorBloco && !parar)
            {
                __carregaRecordDoBloco(record, bloco, nroRecord);
                if((record->TypeVal == TYPEVAL_REGULAR || record->TypeVal == TYPEVAL_DIRETORIO))
                {
                    if(strcmp(record->name,filename) == 0)
                    {
                        parar = true; // Encontrou o arquivo para ser removido
                        if(record->TypeVal != tipo)
                            return -1;

                        __limpaRecord(record);
                        if(__gravaRecord(record, bloco, nroRecord, nroBloco) != 0)
                            return -1;
                    }
                    else
                        vazio = false;
                    nroRecord++;
                }
                else
                    nroRecord++;
            }
            if(vazio)
            {
                atualizaBitmapBlocos(inode->dataPtr[ptr], 0);
                inode->dataPtr[ptr] = TYPEVAL_INVALIDO;
            }
        }
        ptr++;
    }

    /**********************************************************/
    /*** TODO -> Carregar os blocos dos ponteiros indiretos ***/
    /**********************************************************/

    free(record);
    free(inode);
    free(bloco);
    return 0;
}


//Função que carrega o Mapa de Bits de blocos de dados para a variável global BitMap_blocos.
//Retorno: 0=Sucesso; -1=Erro
int loadBitMapBlocks ()
{
	BitMap_Blocos = malloc(superBloco->BlockSize);

    if(le_bloco(superBloco->BitmapBlocks, (char *)BitMap_Blocos) !=0)
        return erroMensagem("Erro ao carregar o bitmap de blocos de dados!", "loadBitMapBlocks ()");

	return 0;
}


//Função que carrega o Mapa de Bits de inodes de dados para a variável global BitMap_inodes.
//Retorno: 0=Sucesso; -1=Erro
int loadBitMapInodes ()
{
	BitMap_inodes = malloc(superBloco->BlockSize);

    if(le_bloco(superBloco->BitmapInodes, (char *)BitMap_inodes) !=0)
        return erroMensagem("Erro ao carregar o bitmap de inodes!", "loadBitMapInodes ()");

	return 0;
}


/// //////////////////////////////////////////////////////////////////////////////
/// Alterado a lógica desta função! Caso ela encontre um arquivo com o mesmo nome,
/// retorna 0 e carrega o record deste arquivo. Feito isso para reaproveitá-la.
/// Se não for necessário carregar um record, passar o NULL como parâmtro do recordDest
/// //////////////////////////////////////////////////////////////////////////////
int check_filename(char *filename, int dirInode, struct t2fs_record *recordDest)
{
    struct t2fs_inode *inode = malloc(sizeof(struct t2fs_inode));

    if(carregaInode(inode, dirInode) != 0)
        return -1;

    BYTE *bloco = malloc(superBloco->BlockSize);

    int ptr = 0;
    bool parar = false;

    while(ptr < 10 && !parar)
    {
        if(inode->dataPtr[ptr] != PTR_INVALIDO)
        {
            int nroBloco = inode->dataPtr[ptr];

            if(le_bloco(nroBloco, (char *) bloco))
                return -1;

            struct t2fs_record *record = malloc(sizeof(struct t2fs_record));

            int nroRecord=0;
            while(nroRecord < recordPorBloco && !parar)
            {
                __carregaRecordDoBloco(record, bloco, nroRecord);
                if((record->TypeVal == TYPEVAL_REGULAR || record->TypeVal == TYPEVAL_DIRETORIO))
                {
                    if(strcmp(record->name,filename) == 0)
                    {
                        if(recordDest != NULL)
                            copiaByteAByte((char *) recordDest, (char *)record, 0, sizeof(struct t2fs_record));
                        free(record);
                        return 0;
                    }
                }
                nroRecord++;

            }

            free(record);
        }
        ptr++;
    }

    /***********************************************************/
    /*** TODO -> Verificar os blocos dos ponteiros indiretos ***/
    /***********************************************************/

    free(inode);
    free(bloco);
    return -1;
}


int buscaRecordPorInode(int inodeBusca, int dirInode, struct t2fs_record *recordDest)
{
    struct t2fs_inode *inode = malloc(sizeof(struct t2fs_inode));

    if(carregaInode(inode, dirInode) != 0)
        return erroMensagem("Erro ao carregar o inode!", "check_filename");

    BYTE *bloco = malloc(superBloco->BlockSize);

    int ptr = 0;
    bool parar = false;

    while(ptr < 10 && !parar)
    {
        if(inode->dataPtr[ptr] != PTR_INVALIDO)
        {
            int nroBloco = inode->dataPtr[ptr];

            if(le_bloco(nroBloco, (char *) bloco))
                return erroMensagem("Erro ao ler o bloco!", "check_filename");

            struct t2fs_record *record = malloc(sizeof(struct t2fs_record));

            int nroRecord=0;
            while(nroRecord < recordPorBloco && !parar)
            {
                __carregaRecordDoBloco(record, bloco, nroRecord);
                if((record->TypeVal == TYPEVAL_REGULAR || record->TypeVal == TYPEVAL_DIRETORIO))
                {
                    if(record->i_node == inodeBusca)
                    {
                        if(recordDest != NULL)
                            copiaByteAByte((char *) recordDest, (char *)record, 0, sizeof(struct t2fs_record));
                        free(record);
                        free(bloco);
                        free(inode);
                        return 0;
                    }
                }
                nroRecord++;

            }

            free(record);
        }
        ptr++;
    }

    /***********************************************************/
    /*** TODO -> Verificar os blocos dos ponteiros indiretos ***/
    /***********************************************************/

    free(inode);
    free(bloco);
    return -1;
}


// Função copia record de número nroRecord do bloco para a variável record
//Retorno: não há retorno
void __carregaRecordDoBloco(struct t2fs_record *recordDest, BYTE *bloco, int nroRecord)
{
    copiaByteAByte((char *) recordDest, (char *) bloco, nroRecord*sizeof(struct t2fs_record), sizeof(struct t2fs_record));
}


// Copia cada byte da variavel orig a partir para a variável fim
// Utilize a variável "inicio" é a partir de onde começa a copiar e "fim" quando deve parar
void copiaByteAByte(char* dest, char* orig, int inicio, int fim)
{
    int i;
    for(i = 0; i < fim; i++)
    {
        dest[i] = orig[inicio+i];
    }
}


// Função que seta o valor do buffer para 0
//Retorno: não há retorno
void limpaBuffer(char * buffer, int tamBloco)
{
    int i;
    for(i = 0; i < tamBloco; i++)
        buffer[i] = 0;
}


// Função para abrir arquivo
//Retorno: de 0 a 19 = handle do arquivo, -1 = erro
int abreArquivo(char *fileName, int nroInodeDiretorio)
{
    int i = 0;
    for(i = 0; i < 20; i++)
    {
        if(!arqAberto[i].aberto)
        {

            struct t2fs_record *recordArq = malloc(sizeof(struct t2fs_record));
            if(check_filename(fileName, nroInodeDiretorio, recordArq) != 0)
                return erroMensagem("Arquivo nao encontrado!", "abreArquivo");

            if(recordArq->TypeVal != TYPEVAL_REGULAR)
                return erroMensagem("O nome informado nao eh de um arquivo!", "abreArquivo");

            arqAberto[i].aberto = true;
            copiaByteAByte((char *) &(arqAberto[i].record), (char *) recordArq, 0, sizeof(struct t2fs_record));
            arqAberto[i].offset = 0;
            arqAberto[i].nroInodeDir = nroInodeDiretorio;

            break;
        }
    }
    return i;
}


// Função retorna a primeira string antes do primeiro /.
char *cutPath (char *pathname)
{
    return strtok(pathname, "/");
}


//Função que cria um novo diretório
//Retorno: 0 = diretório criado com sucesso, -1 =  erro
int criaDiretorio(char *path)
{
    int inodeDirPai;
    char pathTemp[strlen(path)+1];
    strcpy(pathTemp, path);

    // Caminho Absoluto
    if(pathTemp[0] == '/')
    {
        inodeDirPai = 0;
    }
    // Caminho Relativo
    else
    {
        inodeDirPai = nroInodeDirAtual;
    }

    struct t2fs_record *recordDirPai = malloc(sizeof(struct t2fs_record));
    char *aux;
    char *nomeDir = cutPath(pathTemp);
    while(nomeDir != NULL)
    {
        aux = cutPath(NULL);
        if(aux == NULL)
            break;
        else
        {
            if(check_filename(nomeDir, inodeDirPai, recordDirPai) != 0)
            {
                printf("Diretorio '%s' do caminho '%s' nao encontrado!", nomeDir, path);
                return -1;
            }
            if(recordDirPai->TypeVal != TYPEVAL_DIRETORIO)
            {
                printf("'%s' nao eh um diretorio! Caminho '%s' eh invalido!", nomeDir, path);
                return -1;
            }
            nomeDir = aux;
            inodeDirPai = recordDirPai->i_node;
        }
    }

    if(validaNomeArquivo(nomeDir) != 0) return erroMensagem("Nome do diretorio eh invalido!", "criaDiretorio");

    if(check_filename(nomeDir, inodeDirPai, NULL) == 0) return erroMensagem("Ja existe um arquivo/diretorio com este nome no mesmo diretorio!", "criaDiretorio");

    struct t2fs_record *recordNovoDir = malloc(sizeof(struct t2fs_record));
    if(iniciaRecordDiretorio(recordNovoDir, TYPEVAL_DIRETORIO, nomeDir, inodeDirPai) != 0) return -1;

    free(recordNovoDir);
    free(recordDirPai);
    return 0;
}


//Função que inicializa o record de um diretório.
//Retorno: 0 = record inicializado com sucesso, -1 = erro
int iniciaRecordDiretorio(struct t2fs_record *record, BYTE type, char *filename, DWORD nroInodePai)
{
    if(iniciaNovoRecord(record, type, filename) != 0) return -1;
    record->blocksFileSize = 1;
    record->bytesFileSize = 1024;
    if(gravaRecord(record, nroInodePai) != 0) return -1;

    struct t2fs_record *recordDir = malloc(sizeof(struct t2fs_record));
    if(iniciaNovoRecord(recordDir, TYPEVAL_DIRETORIO, ".") != 0) return -1;
    if(removeInode(recordDir->i_node) != 0) return -1; // Para aproveitar a função iniciaNovoRecord é necessário remover o i_node criado automaticamente

    recordDir->i_node = record->i_node;
    recordDir->blocksFileSize = 1;
    recordDir->bytesFileSize = 1024;
    if(gravaRecord(recordDir, record->i_node) != 0) return -1;


    strcpy(recordDir->name, "..");
    recordDir->i_node = nroInodePai;
    if(gravaRecord(recordDir, record->i_node) != 0) return -1;

    free(recordDir);

    return 0;
}


int removeDiretorio(char *path)
{
    int inodeDirPai;
    char pathTemp[strlen(path)+1];
    strcpy(pathTemp, path);

    // Caminho Absoluto
    if(pathTemp[0] == '/')
    {
        inodeDirPai = 0;
    }
    // Caminho Relativo
    else
    {
        inodeDirPai = nroInodeDirAtual;
    }

    struct t2fs_record *recordDirPai = malloc(sizeof(struct t2fs_record));
    char *aux;
    char *nomeDir = cutPath(pathTemp);
    while(nomeDir != NULL)
    {
        aux = cutPath(NULL);
        if(aux == NULL)
            break;
        else
        {
            if(check_filename(nomeDir, inodeDirPai, recordDirPai) != 0)
                return erroMensagem("Caminho nao encontrado", "removeDiretorio");

            if(recordDirPai->TypeVal != TYPEVAL_DIRETORIO)
                return erroMensagem("Caminho invalido!", "removeDiretorio");

            nomeDir = aux;
            inodeDirPai = recordDirPai->i_node;
        }
    }

    if(strcmp(nomeDir, ".") == 0 || strcmp(nomeDir, "..") == 0) return erroMensagem("Nao eh possivel apagar diretorios '.' e '..'!", "removeDiretorio");

    struct t2fs_record *recordDirRemover = malloc(sizeof(struct t2fs_record));
    if(check_filename(nomeDir, inodeDirPai, recordDirRemover) != 0) return erroMensagem("Nao existe nenhum diretorio com este nome!", "removeDiretorio");

    if(recordDirRemover->TypeVal != TYPEVAL_DIRETORIO) return erroMensagem("Caminho informado nao eh de um diretorio!", "removeDiretorio");

    if(contaArquivos(recordDirRemover->i_node, false) > 2) return erroMensagem("Nao eh possivel apagar o diretorio porque ele nao esta vazio!", "removeDiretorio");

    if(removeRecord(recordDirRemover->name, inodeDirPai, TYPEVAL_DIRETORIO) != 0) return -1;

    free(recordDirRemover);
    free(recordDirPai);
    return 0;
}


int abreDiretorio(char *path)
{
    int inodeDirPai;
    char pathTemp[strlen(path)+1];
    strcpy(pathTemp, path);

    // Caminho Absoluto
    if(pathTemp[0] == '/')
    {
        inodeDirPai = 0;
    }
    // Caminho Relativo
    else
    {
        inodeDirPai = nroInodeDirAtual;
    }

    struct t2fs_record *recordDirPai = malloc(sizeof(struct t2fs_record));
    char *aux;
    char *nomeDir = cutPath(pathTemp);
    while(nomeDir != NULL)
    {
        aux = cutPath(NULL);
        if(aux == NULL)
            break;
        else
        {
            if(check_filename(nomeDir, inodeDirPai, recordDirPai) != 0)
            {
                printf("Diretorio '%s' do caminho '%s' nao encontrado!", nomeDir, path);
                return -1;
            }
            if(recordDirPai->TypeVal != TYPEVAL_DIRETORIO)
            {
                printf("'%s' nao eh um diretorio! Caminho '%s' eh invalido!", nomeDir, path);
                return -1;
            }
            nomeDir = aux;
            inodeDirPai = recordDirPai->i_node;
        }
    }

    // Caso diretorio raiz
    if(nomeDir == NULL)
    {
         strcpy(pathTemp, ".");
         nomeDir = pathTemp;
    }

    struct t2fs_record *recordDirAberto = malloc(sizeof(struct t2fs_record));
    if(check_filename(nomeDir, inodeDirPai, recordDirAberto) != 0) return erroMensagem("Nao existe nenhum diretorio com este nome!", "abreDiretorio");

    if(recordDirAberto->TypeVal != TYPEVAL_DIRETORIO) return erroMensagem("Caminho informado nao eh de um diretorio!", "abreDiretorio");

    int i = 0;
    for(i = 0; i < 20; i++)
    {
        if(!dirAberto[i].aberto)
        {
            dirAberto[i].aberto = true;
            copiaByteAByte((char *) &(dirAberto[i].record), (char *) recordDirAberto, 0, sizeof(struct t2fs_record));
            dirAberto[i].offset = 0;
            dirAberto[i].nroInodeDirPai = inodeDirPai;
            break;
        }
    }

    free(recordDirAberto);
    free(recordDirPai);
    return i;
}


int carregaDentry(DIR2 handle, DIRENT2 *dentry)
{
    int offset = dirAberto[handle].offset;
    int nthBloco = 0;
    while(offset >= recordPorBloco)
    {
        offset -= recordPorBloco;
        nthBloco++;
    }

    DWORD nroBloco = achaBloco(nthBloco, dirAberto[handle].record);

    BYTE *bloco = malloc(superBloco->BlockSize);
    if(le_bloco(nroBloco, (char *) bloco) != 0) return -1;

    struct t2fs_record *record = malloc(sizeof(struct t2fs_record));
    copiaByteAByte((char *) record, (char *) &bloco[offset*(sizeof(struct t2fs_record))], 0, sizeof(struct t2fs_record));

    if(record->TypeVal != TYPEVAL_REGULAR && record->TypeVal != TYPEVAL_DIRETORIO)
        return -1;

    strcpy(dentry->name, record->name);
    dentry->fileType = record->TypeVal;
    dentry->fileSize = record->bytesFileSize;

    dirAberto[handle].offset++;

    free(record);
    free(bloco);
    return 0;
}


int alteraDiretorioAtual(char *path)
{
    int inodeDirPai;
    char pathTemp[strlen(path)+1];
    strcpy(pathTemp, path);

    // Caminho Absoluto
    if(pathTemp[0] == '/')
    {
        inodeDirPai = 0;
    }
    // Caminho Relativo
    else
    {
        inodeDirPai = nroInodeDirAtual;
    }

    struct t2fs_record *recordDirPai = malloc(sizeof(struct t2fs_record));
    char *aux;
    char *nomeDir = cutPath(pathTemp);
    while(nomeDir != NULL)
    {
        aux = cutPath(NULL);
        if(aux == NULL)
            break;
        else
        {
            if(check_filename(nomeDir, inodeDirPai, recordDirPai) != 0)
            {
                printf("Diretorio '%s' do caminho '%s' nao encontrado!", nomeDir, path);
                return -1;
            }
            if(recordDirPai->TypeVal != TYPEVAL_DIRETORIO)
            {
                printf("'%s' nao eh um diretorio! Caminho '%s' eh invalido!", nomeDir, path);
                return -1;
            }
            nomeDir = aux;
            inodeDirPai = recordDirPai->i_node;
        }
    }

    // Caso diretorio raiz
    if(nomeDir == NULL)
    {
         strcpy(pathTemp, ".");
         nomeDir = pathTemp;
    }

    struct t2fs_record *recordDirAberto = malloc(sizeof(struct t2fs_record));
    if(check_filename(nomeDir, inodeDirPai, recordDirAberto) != 0) return erroMensagem("Nao existe nenhum diretorio com este nome!", "abreDiretorio");

    if(recordDirAberto->TypeVal != TYPEVAL_DIRETORIO) return erroMensagem("Caminho informado nao eh de um diretorio!", "abreDiretorio");

    nroInodeDirAtual = recordDirAberto->i_node;

    free(recordDirAberto);
    free(recordDirPai);
    return 0;
}


int caminhoDirAtual(char *pathname, int size)
{
    char tempCaminho[1024];
    char tempAux[1024];

    strcpy(tempCaminho, "\0");
    strcpy(tempAux, "\0");

    struct t2fs_record *recordPai = malloc(sizeof(struct t2fs_record));
    struct t2fs_record *recordFilho = malloc(sizeof(struct t2fs_record));
    struct t2fs_inode *inode = malloc(sizeof(struct t2fs_inode));

    BYTE *bloco = malloc(superBloco->BlockSize);

    int nroInode = nroInodeDirAtual;

    while(nroInode != 0)
    {
        carregaInode(inode, nroInode);
        check_filename("..", nroInode, recordPai);

        if(buscaRecordPorInode(nroInode, recordPai->i_node, recordFilho) != 0) return -1;

        strcpy(tempAux, recordFilho->name);
        strcat(tempAux, "/");
        strcat(tempAux, tempCaminho);
        strcpy(tempCaminho, tempAux);

        nroInode = recordPai->i_node;
    }

    strcpy(tempAux, "/");
    strcat(tempAux, tempCaminho);
    strcpy(tempCaminho, tempAux);

    strncpy(pathname, tempCaminho, size);

    bool barraZero = false;
    int i = 0;
    while(i < size)
    {
        if(pathname[i] == '\0')
        {
            barraZero = true;
            break;
        }
        i++;
    }

    if(!barraZero)
        return erroMensagem("Tamanho do size eh insuficiente!", "caminhoDirAtual");

    free(recordFilho);
    free(recordPai);
    free(inode);
    free(bloco);
    return 0;
}




//Funão que encontra o apontador de um bloco de dados dado seu número relativo no arquivo.
//Retorno: -1 em caso de erro, o apontador direto do bloco caso contrario
DWORD achaBloco (int bloco, struct t2fs_record record)
{
    struct t2fs_inode *inode = malloc(sizeof(struct t2fs_inode));

    if(carregaInode(inode, record.i_node) !=0) return erroMensagem("Erro ao carregar inode!", "achaBloco");

    if(bloco<10) //Se o bloco está nos ponteiros diretos
    {
        int nroBloco = inode->dataPtr[bloco];
        free(inode);
        return nroBloco;
    }
    else if(bloco<(maxPtrIndiretosPorBloco)+10) //Se bloco está nos ponteiros indiretos simples
    {
        int *blocoPonteiro = malloc(sizeof(int)*maxPtrIndiretosPorBloco);
        if(le_bloco(inode->singleIndPtr, (char *)blocoPonteiro)!=0) return erroMensagem("Erro ao carregar bloco de indireção simples!", "achaBloco");
        return blocoPonteiro[(bloco-10)];
    }
    else //Se bloco está nos ponteiros indiretos duplos
    {
        int *blocoPonteiroDup = malloc(sizeof(int)*maxPtrIndiretosPorBloco);
        if(le_bloco(inode->doubleIndPtr, (char *)blocoPonteiroDup)!=0) return erroMensagem("Erro ao carregar bloco de indireçãodupla!", "achaBloco");

        int blocoIndSimp=-1, BlocoDir=0;
        int current = maxPtrIndiretosPorBloco+10;

        while(bloco>=current)
        {
            current+=maxPtrIndiretosPorBloco;
            blocoIndSimp++;
        }

        int *blocoInd = malloc(sizeof(int)*maxPtrIndiretosPorBloco);
        if(le_bloco(blocoIndSimp++, (char *)blocoPonteiroDup)!=0) return erroMensagem("Erro ao carregar bloco secundario de ponteiros!", "achaBloco");

        current=10+maxPtrIndiretosPorBloco+(blocoIndSimp*maxPtrIndiretosPorBloco);

        while(bloco>current)
        {
            current++;
            BlocoDir++;
        }

        return blocoInd[BlocoDir];
    }

    return -1;
}


//Função que lê um número (size) de bytes de um arquivo e coloca esses dados em buffer.
//Retorno: -1 em caso de erro, ou o número de bytes lidos em caso de sucesso.
int leArquivo(FILE2 handle, char *buffer, int size)
{
//    int tamArquivo = arqAberto[handle].record.bytesFileSize;

    int offset = arqAberto[handle].offset;
    int nthBloco = 0;
    while(offset >= superBloco->BlockSize)
    {
        offset -= superBloco->BlockSize;
        nthBloco++;
    }

    DWORD nroBloco = achaBloco(nthBloco, arqAberto[handle].record);

    BYTE *bloco = malloc(superBloco->BlockSize);
    if(le_bloco(nroBloco, (char *) bloco) != 0) return -1;

    int bytesLidos= 0;
    while(bytesLidos < size && arqAberto[handle].offset < arqAberto[handle].record.bytesFileSize)
    {
        // Limite do bloco. Será necessário alocar um novo bloco
        if(offset >= superBloco->BlockSize)
        {
            offset -= superBloco->BlockSize;

            nthBloco++;
            nroBloco = achaBloco(nthBloco, arqAberto[handle].record);
            if(le_bloco(nroBloco, (char *) bloco) != 0) return -1;
        }
        copiaByteAByte(&buffer[bytesLidos], (char *) bloco, offset, 1);

        bytesLidos++;
        offset++;
        arqAberto[handle].offset++;
    }

    free(bloco);

    return bytesLidos;
}


//Função que escreve em u arquivo.
//Retorno: -1 em caso de erro, numero de bytes gravados em caso de sucesso.
int escreveNoArquivo(FILE2 handle, char *buffer, int size)
{
    int offset = arqAberto[handle].offset;
    int nthBloco = 0;
    while(offset >= superBloco->BlockSize)
    {
        offset -= superBloco->BlockSize;
        nthBloco++;
    }

    if(nthBloco >= arqAberto[handle].record.blocksFileSize)
        if(alocaNovoBlocoDados(&arqAberto[handle].record) != 0) return -1;

    DWORD nroBloco = achaBloco(nthBloco, arqAberto[handle].record);

    BYTE *bloco = malloc(superBloco->BlockSize);
    if(le_bloco(nroBloco, (char *) bloco) != 0) return -1;

    int bytesGravados = 0;
    while(bytesGravados < size)
    {
        // Limite do bloco. Será necessário alocar um novo bloco
        if(offset >= superBloco->BlockSize)
        {
            offset -= superBloco->BlockSize;
            if(grava_bloco(nroBloco, (char *) bloco) != 0) return -1;
            if(alocaNovoBlocoDados(&arqAberto[handle].record) != 0) return -1;

            nthBloco++;
            nroBloco = achaBloco(nthBloco, arqAberto[handle].record);
            if(le_bloco(nroBloco, (char *) bloco) != 0) return -1;
        }
        copiaByteAByte((char *) &bloco[offset], buffer, bytesGravados, 1);

        bytesGravados++;
        offset++;
        arqAberto[handle].offset++;
        if(arqAberto[handle].offset > arqAberto[handle].record.bytesFileSize)
            arqAberto[handle].record.bytesFileSize++;
    }

    grava_bloco(nroBloco, (char *)bloco);
    if(atualizaRecord(&arqAberto[handle].record, arqAberto[handle].nroInodeDir) != 0) return erroMensagem("Erro atualizar o record", "escreveNoArquivo");

    return bytesGravados;
}


//Função que atualiza o record no diretório de inode nroInodeDir
//Retorno: 0 = sucesso na atualização do record, -1 = erro
int atualizaRecord(struct t2fs_record *record, int nroInodeDir)
{
    if(removeRecord(record->name, nroInodeDir, TYPEVAL_REGULAR) != 0) return -1;
    if(gravaRecord(record, nroInodeDir) != 0) return -1;
    return 0;
}


// Função utilizada para alocar bloco de dados para o arquivo.
//Retorno: 0 = sucesso ao alocar novo bloco, -1 = erro
int alocaNovoBlocoDados(struct t2fs_record *record)
{
    struct t2fs_inode *inode = malloc(sizeof(struct t2fs_inode));
    if(carregaInode(inode, record->i_node) != 0) return -1;

    int ptr = 0;
    while(inode->dataPtr[ptr] != PTR_INVALIDO && ptr < 10)
    {
        ptr++;
    }

    if(ptr < 10)
    {
        int nroBloco = blocoDadosLivre();
        if(nroBloco == -1) return -1;

        inode->dataPtr[ptr] = nroBloco;

        if(gravaInode(inode, record->i_node) != 0) return -1;
        if(atualizaBitmapBlocos(nroBloco, 1) != 0) return -1;
        record->blocksFileSize++;
        return 0;
    }
    else
    {
        BYTE *bloco = malloc(superBloco->BlockSize);
        BYTE *temp = malloc(superBloco->BlockSize);

        //Indireção simples
        if(le_bloco(inode->singleIndPtr, (char *) bloco))
                return erroMensagem("Erro ao ler o bloco!", "alocaNovosBlocosDados");

        int i=0;

        while(i<maxPtrIndiretosPorBloco)
        {
            if(bloco[i]==PTR_INVALIDO)
            {
                int nroBloco = blocoDadosLivre();
                if(nroBloco == -1) return -1;

                bloco[i] = nroBloco;

                if(grava_bloco(inode->singleIndPtr,(char *)bloco)!=0) return -1;
                if(atualizaBitmapBlocos(nroBloco, 1) != 0) return -1;
                record->blocksFileSize++;
                return 0;
            }
            i++;
        }

        //Indireção dupla
        if(le_bloco(inode->doubleIndPtr, (char *) temp))
                return erroMensagem("Erro ao ler o bloco!", "alocaNovosBlocosDados");

        i=0;

        int j=0;

        while(j<maxPtrIndiretosPorBloco)
        {
            if(temp[j]!=PTR_INVALIDO)
            {
                if(le_bloco(temp[j], (char *) bloco))
                    return erroMensagem("Erro ao ler o bloco!", "alocaNovosBlocosDados");
                while(i<maxPtrIndiretosPorBloco)
                {
                    if(bloco[i]==PTR_INVALIDO)
                    {
                        int nroBloco = blocoDadosLivre();
                        if(nroBloco == -1) return -1;

                        bloco[i] = nroBloco;

                        if(grava_bloco(temp[j],(char *)bloco)!=0) return -1;
                        if(atualizaBitmapBlocos(nroBloco, 1) != 0) return -1;
                        record->blocksFileSize++;
                        return 0;
                    }
                    i++;
                }
            }
            else //temp[j] é vazio, precisa alocar 2 blocos
            {
                int nroBloco = blocoDadosLivre();
                if(nroBloco == -1) return -1;

                temp[j] = nroBloco;

                if(grava_bloco(inode->doubleIndPtr,(char *)temp)!=0) return -1;
                if(atualizaBitmapBlocos(nroBloco, 1) != 0) return -1;

                if(le_bloco(temp[j], (char *) bloco))
                    return erroMensagem("Erro ao ler o bloco!", "alocaNovosBlocosDados");

                nroBloco = blocoDadosLivre();
                if(nroBloco == -1) return -1;

                bloco[0]=nroBloco;

                if(grava_bloco(temp[j],(char *)bloco)!=0) return -1;
                if(atualizaBitmapBlocos(nroBloco, 1) != 0) return -1;

                record->blocksFileSize++;
                return 0;
            }
            j++;
        }


        free(bloco);
        free(temp);
        /***************************************************/
        /*** TODO -> Criar bloco nos ponteiros indiretos: FEEEEITOOOOO ***/
        /***************************************************/
        return -1;
    }
}


    /// ///////////////////////////// ///
    /// Funções de impressão de dados ///
    /// ///////////////////////////// ///

// Função utilizada para mostrar a configuração dos bits do Mapa de Bits de blocos. Retorno: 0 = função terminada com sucesso; -1 = erro de leitura. Imprime a configuração dos bits.
int statusBitmapBlocks()
{
    int i = 0;
    printf("\nStatus do Bitmap de Blocos\n");
    for(i = 0; i < bytesBitmapBlocos; i++)
    {
        printf("%u ", BitMap_Blocos[i]);
    }
//    printf("\nDigite um numero para continuar: ");int t; scanf("%d", &t);

	return 0;
}


// Função utilizada para mostrar a configuração dos bits do Mapa de Bits de inodes. Retorno: 0 = função terminada com sucesso; -1 = erro de leitura. Imprime a configuração dos bits.
int statusBitmapInodes()
{
    int i = 0;
    printf("\nStatus do Bitmap de Inodes\n");
    for(i = 0; i < bytesBitmapInodes; i++)
    {
        printf("%u ", BitMap_inodes[i]);
    }
//    printf("\nDigite um numero para continuar: ");int t; scanf("%d", &t);

    return 0;
}


// Função retorna o número do primeiro bloco de dados livre,
// ou -1 caso não há blocos livres
int blocoDadosLivre()
{
    int posLivre;
    posLivre = bitLivre(BitMap_Blocos, bytesBitmapBlocos);

    return posLivre;
}


// Função retorna o número do primeiro inode livre (cuidado, não é bloco),
// ou -1 caso não há blocos livres
int inodeLivre()
{
    int posLivre;

    posLivre = bitLivre(BitMap_inodes, bytesBitmapInodes);

    return posLivre;
}


// Retorna a posição do primeiro bit igual a zero de um bitmap,
// ou -1 caso não tenha mais bit livre.
// Recebe como parâmetros o bitmap e o número máximo de bits utilizado pelo mesmo
int bitLivre(BYTE *bitmap, int max)
{
    int i,j;
    int bit;
    BYTE  aux;
    for(i = 0; i < max; i++)
    {
        aux = bitmap[i];
        for(j = 0; j < 8; j++)
        {
            bit = ((aux >> j) & 0x001);
            if(bit == 0) return i*8+j;
        }
    }
    return erroMensagem("Nao ha mais bit livre no bitmap!", "bitLivre(BYTE *bitmap, int max)");
}


// Imprimi os dados do inode
void imprimeInode(struct t2fs_inode *inode)
{
    if(inode == NULL)
        printf("\nInode Vazio!\n");
    else
    {
        printf("\nImprimindo Inode!\n");
        int i;
        for(i = 0; i < 10; i++)
            printf("Ponteiro %d data: %d\n", i, inode->dataPtr[i]);
        printf("Ponteiro simples: %d\n", inode->singleIndPtr);
        printf("Ponteiro duplo: %d\n", inode->doubleIndPtr);
    }
}


void imprimeRecord(struct t2fs_record *record)
{
    if(record == NULL)
        printf("\nRecord Vazio!\n");
    else
    {
        printf("\nImprimindo Record!\n");
        printf("Type: %d\n", record->TypeVal);
        printf("Nome: %s\n", record->name);
        printf("Blocos: %d\n", record->blocksFileSize);
        printf("Bytes: %d\n", record->bytesFileSize);
        printf("Inode: %d\n", record->i_node);
    }
}


// Valida o tamanho do nome do arquivo e se não contêm caracteres especiais
int validaNomeArquivo(char* fileName)
{
    int i;
    for(i = 0; i < 31; i++)
    {
        if(fileName[i] == '\0')
        {
            if(i == 0)
                return erroMensagem("Nome do arquivo nao pode ser vazio!", "validaNomeArquivo");
            else
                return 0;
        }

        if(fileName[i] < 0x21 || fileName[i] > 0x7A )
            return erroMensagem("Nome do arquivo com caractere invalido!", "validaNomeArquivo");

        if(fileName[i] == '/')
            return erroMensagem("Nome do arquivo nao pode conter '/'!", "validaNomeArquivo");
    }
    return erroMensagem("Nome do arquivo invalido!", "validaNomeArquivo");
}


// Informa a mensagem informada e a funcao que ocorreu o erro e retorna -1
int erroMensagem(char * mensagem, char *funcao)
{
    printf("\n%s\n", mensagem);
    #ifdef AUX_DEBUG
    printf("Funcao: %s\n", funcao);
    #endif
    return -1;
}


