#include "../include/apidisk.h"
#include "../include/t2fs.h"
#include "../include/auxiliar.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

int nroArqAberto = 0;
int nroDirAberto = 0;

bool superblocoIniciado = false;
BYTE *BitMap_blocos;
BYTE *BitMap_inodes;

int iniciaSuperbloco();




/// Implementação das funções da t2fc.h
int identify2 (char *name, int size)
{
    char id[] = "Bruno Freitas (173013), Carolina Eberhart (207046)";
	if (size < sizeof(id))
		return -1;
	else strncpy(name, id, sizeof(id));
	return 0;
}


//Função que inicializa o super bloco a partir do disco t2fs_disk.dat sendo utilizado.
int iniciaSuperbloco()
{
    if (!superblocoIniciado) {
        if (leSuperbloco() != 0) return erroMensagem("Erro ao iniciar o Super Bloco!", "iniciaSuperbloco");;
        superblocoIniciado = true;
    }
    return 0;
}


FILE2 create2 (char *filename)
{
	//Inicializa SuperBloco
    if(iniciaSuperbloco() != 0) return -1;
	//Testa se já existem 20 arquivos abertos
    if (nroArqAberto == 20) return erroMensagem("Numero de maximo de arquivos aberto!", "create2");
	// Verifica se o nome do arquivo é válido
	if(validaNomeArquivo(filename) == -1) return -1;
	//Testa se há i-nodes disponíveis
	if (inodeLivre() == -1) return -1;
	//Testa se há blocos de dados disponíveis
	if (blocoDadosLivre() == -1) return -1;
	//Testa se o nome do arquivo passado pelo usuário está disponível
	if (check_filename(filename, nroInodeDirAtual, NULL) == 0) return erroMensagem("Ja existe um arquivo com o mesmo nome!", "create2");


	struct t2fs_record *record = malloc(sizeof(struct t2fs_record));
    if(iniciaNovoRecord(record, TYPEVAL_REGULAR, filename) != 0) return -1;

    if(gravaRecord(record, nroInodeDirAtual) != 0) return -1;

    int handle = open2(record->name);
    free(record);

    return handle;
}


int delete2 (char *filename)
{
    int i=0;
	//Inicializa SuperBloco
    if(iniciaSuperbloco() != 0) return -1;
	// Verifica se o nome do arquivo é válido
	if(validaNomeArquivo(filename) == -1) return -1;
	// Valida se o arquivo existe
	if (check_filename(filename, nroInodeDirAtual, NULL) == -1) return erroMensagem("Arquivo não existe!", "delete2");


    for(i=0; i<20; i++)
    {
        if(arqAberto[i].aberto == true)
        {
            if(strcmp(arqAberto[i].record.name, filename) == 0)
                return erroMensagem("Nao eh possivel deletar o arquivo porque ele esta aberto!", "delete2");
        }
    }

	if(removeRecord(filename, nroInodeDirAtual, TYPEVAL_REGULAR) == -1) return erroMensagem("Erro ao remover o arquivo!", "delete2");

	return 0;
}


FILE2 open2 (char *filename)
{
    //Inicializa SuperBloco
    if(iniciaSuperbloco() != 0) return -1;
    // Verifica se pode abrir um novo arquivo
    if(nroArqAberto == 20) return erroMensagem("Numero maximo de arquivos abertos!", "open2");
    // Verifica se o nome do arquivo é válido
	if(validaNomeArquivo(filename) == -1) return -1;
	// Valida se o arquivo existe
    int handle = abreArquivo(filename, nroInodeDirAtual);
    if(handle == -1)
        return -1;

    nroArqAberto++;

	return handle;
}


int close2 (FILE2 handle)
{
    //Checa se o superbloco foi inicializado
    if(iniciaSuperbloco() != 0) return -1;
    //Checa se o handle dado é válido
    if (handle < 0 || handle >= 20) return erroMensagem("Handle invalido!", "close2");
    //Checa se o arquivo está de fato aberto
    if (arqAberto[handle].aberto == false) return erroMensagem("Arquivo já foi fechado!", "close2");

    //Fecha o arquivo (faz com que seu handle seja invalido)
    nroArqAberto--;
    arqAberto[handle].aberto = false;
    return 0;
}


int read2 (FILE2 handle, char *buffer, int size)
{
    //Checa se o handle é válido
    if(handle < 0 || handle >= 20) return erroMensagem("Handle invalido!", "read2");
    //Inicializa SuperBloco
    if(iniciaSuperbloco() != 0) return -1;
    //Verifica se o arquivo está aberto
    if (arqAberto[handle].aberto == false) return erroMensagem("Arquivo já foi fechado!", "read2");
    //Checa se size é menor que 1
    if(size < 1) return erroMensagem("Size inválido!", "read2");
    //Checa se o numero de bytes é válido (de acordo com o tamanho máximo de bytes de um arquivo)
    if(size > nroMaxBytes) return erroMensagem("Size inválido!", "read2");

    int bytesLidos = leArquivo(handle, buffer, size);
    if(bytesLidos == -1) return erroMensagem("Erro ao ler o arquivo", "read2");

    return bytesLidos;
}


int write2 (FILE2 handle, char *buffer, int size)
{
    //Checa se o handle é válido
    if(handle < 0 || handle >= 20) return erroMensagem("Handle invalido!", "write2");
    //Checa se size é menor que 1
    if(size < 1) return erroMensagem("Size inválido!", "write2");
    //Checa se o numero de bytes é válido (de acordo com o tamanho máximo de bytes de um arquivo)
    if(size > nroMaxBytes) return erroMensagem("Size inválido!", "write2");

    if(!arqAberto[handle].aberto) return erroMensagem("O handle informado nao eh de um arquivo aberto!", "write2");

    int bytesEscritos = escreveNoArquivo(handle, buffer, size);
    if(bytesEscritos == -1) return erroMensagem("Erro ao escrever no arquivo", "write2");

    return bytesEscritos;
}


int seek2(FILE2 handle, unsigned int offset)
{
    //Checa se o handle é válido
    if(handle < 0 || handle >= 20) return erroMensagem("Handle invalido!", "seek2");

    if(arqAberto[handle].aberto == false) return erroMensagem("o arquivo esta fechado! Nao eh possivel alterar o offset!", "seek2");

    // Posiciona no final do arquivo
    if(offset == -1 || offset > arqAberto[handle].record.bytesFileSize)
    {
        arqAberto[handle].offset = arqAberto[handle].record.bytesFileSize-1; // -1 por causa do \0
    }
    else
    {
        arqAberto[handle].offset = offset;
    }

    return 0;
}


int mkdir2 (char *pathname)
{
	// Inicializa SuperBloco
    if(iniciaSuperbloco() != 0) return -1;
    // Testa se há inodes disponíveis
	if(inodeLivre() == -1) return -1;
	// Testa se há blocos de dados disponíveis
	if(blocoDadosLivre() == -1) return -1;


	if(criaDiretorio(pathname) != 0) return -1;

    return 0;
}


int rmdir2 (char *pathname)
{
    //Inicializa SuperBloco
    if(iniciaSuperbloco() != 0) return -1;

    if(removeDiretorio(pathname) != 0) return -1;

    return 0;
}


DIR2 opendir2 (char *pathname)
{
    //Inicializa SuperBloco
    if(iniciaSuperbloco() != 0) return -1;
    // Verifica se pode abrir um novo arquivo
    if(nroDirAberto == 20) return erroMensagem("Numero maximo de diretorios abertos!", "opendir2");
	// Valida se o arquivo existe
    int handle = abreDiretorio(pathname);
    if(handle == -1)
        return -1;

    nroDirAberto++;

	return handle;
}


int readdir2 (DIR2 handle, DIRENT2 *dentry)
{
    //Checa se o superbloco foi inicializado
    if(iniciaSuperbloco() != 0) return -1;
    //Checa se o handle dado é válido
    if (handle < 0 || handle >= 20) return erroMensagem("Handle invalido!", "readdir2");
    //Checa se o arquivo está de fato aberto
    if (dirAberto[handle].aberto == false) return erroMensagem("Diretorio nao esta aberto!", "readdir2");

    return carregaDentry(handle, dentry);
}


int closedir2 (DIR2 handle)
{
    //Checa se o superbloco foi inicializado
    if(iniciaSuperbloco() != 0) return -1;
    //Checa se o handle dado é válido
    if (handle < 0 || handle >= 20) return erroMensagem("Handle invalido!", "closedir2");
    //Checa se o arquivo está de fato aberto
    if (dirAberto[handle].aberto == false) return erroMensagem("Diretorio já esta fechado!", "closedir2");

    //Fecha o arquivo (faz com que seu handle seja invalido)
    nroDirAberto--;
    dirAberto[handle].aberto = false;
    return 0;
}


int chdir2 (char *pathname)
{
    //Inicializa SuperBloco
    if(iniciaSuperbloco() != 0) return -1;

    if(alteraDiretorioAtual(pathname) != 0) return -1;

    return 0;
}


int getcwd2 (char *pathname, int size)
{
    if(iniciaSuperbloco() != 0) return -1;

    if(size < 1 || size > 1024) return erroMensagem("Size invalido!", "getcwd2");

    if(caminhoDirAtual(pathname, size) != 0) return -1;

    return 0;
}
