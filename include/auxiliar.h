#pragma once
#ifndef AUXILIAR_H
#define AUXILIAR_H

#include "t2fs.h"
#include "apidisk.h"
#include <string.h>

#define true 1
#define false 0
#define bool char

#define PTR_INVALIDO 0x0FFFFFFFF


typedef struct {
    bool aberto;                // Verifica se o arquivo está aberto
    struct t2fs_record record;  // Record contendo os dados do arquivo aberto
    DWORD nroInodeDir;          // Número do inode do diretório que contém o arquivo
    int offset;
//    char caminhoAbs[1024];    // Caminho absoluto até o arquivo
} ARQ_ABERTO;

typedef struct {
    bool aberto;                // Verifica se o arquivo está aberto
    struct t2fs_record record;  // Record contendo os dados do arquivo aberto
    DWORD nroInodeDirPai;          // Número do inode do diretório que contém o Diretorio
    int offset;
//    char caminhoAbs[1024];    // Caminho absoluto até o arquivo
} DIR_ABERTO;

ARQ_ABERTO arqAberto[20];
DIR_ABERTO dirAberto[20];

char diretorioAtual[1024];
int  nroInodeDirAtual; // Recebe o valor do inode do diretório atual. 0 representa o inode da pasta raiz


//Variáveis
struct t2fs_superbloco *superBloco;
int setoresPorBloco;
int maxPtrIndiretosPorBloco;
int nroMaxBlocos;
int setorDiretorioCorrente;
int inodesPorBloco;
int recordPorBloco;
int nroMaxBytes;
char pathNextStep[31];

int bytesBitmapBlocos; // Número necessário de bytes para representar o bitmap de blocos
int bytesBitmapInodes; // Número necessário de bytes para representar o bitmap de Inodes

BYTE *BitMap_Blocos;
BYTE *BitMap_inodes;


int leSuperbloco();

int grava_bloco(unsigned int bloco, char *buffer);
int le_bloco(unsigned int bloco, char *buffer);
int blocoDadosLivre();
DWORD achaBloco (int bloco, struct t2fs_record record);

int atualizaBitmapInode(int pos, int ocupado);
int atualizaBitmapBlocos(int pos, int ocupado);
int loadBitMapBlocks ();
int loadBitMapInodes ();
int bitLivre(BYTE *bitmap, int max);
void alteraBit(unsigned char *c, int pos, char valor);
void copiaByteAByte(char* dest, char* orig, int inicio, int fim);

int inodeLivre();
int criaInode(int posInode);
int carregaInode(struct t2fs_inode *inode, int nroInode);

int iniciaNovoRecord(struct t2fs_record *record, BYTE type, char *filename);
int gravaRecord(struct t2fs_record *record, int dirInode);
int removeRecord(char* filename, int dirInode, int tipo);
int iniciaRecordDiretorio(struct t2fs_record *record, BYTE type, char *filename, DWORD nroInodePai);
int atualizaRecord(struct t2fs_record *record, int nroInodeDir);
int check_filename(char *filename, int dirInode, struct t2fs_record *recordDest);

int validaNomeArquivo(char* fileName);
void limpaBuffer(char * buffer, int tamBloco);
char *cutPath (char *pathname);
int indirSimples_listaArquivos(int nroBlocoPrt);
int indirDuplo_listaArquivos(int nroBlocoPrt);

int abreArquivo(char *fileName, int nroInodeDiretorio);
int escreveNoArquivo(FILE2 handle, char *buffer, int size);
int leArquivo(FILE2 handle, char *buffer, int size);
int contaArquivos(int dirInode, char imprimir);

int alocaNovoBlocoDados(struct t2fs_record *record);

int criaDiretorio(char *path);
int removeDiretorio(char *path);
int abreDiretorio(char *path);
int alteraDiretorioAtual(char *path);
int carregaDentry(DIR2 handle, DIRENT2 *dentry);
int caminhoDirAtual(char *pathname, int size);

/// Funções de impressão de dados
int statusBitmapBlocks();
int statusBitmapInodes();

void imprimeInode(struct t2fs_inode *inode);
void imprimeRecord(struct t2fs_record *inode);
int erroMensagem(char * mensagem, char *funcao);
#endif
